const name = 'auth'

const state = () => ({
  userInfo: {},
  role: {},
  permissions: []
})

const getters = {
  getUserInfo(state) {
    return state.userInfo
  },
  getRole(state) {
    return state.role
  },
  getPermissions(state) {
    return state.permissions
  },
}

const mutations = {
  SET_USER(state, value) {
    state.userInfo = value
  },
  SET_PERMISSIONS(state, value) {
    state.permissions = value
  },
  SET_ROLE(state, value) {
    state.role = value
  },
}

const actions = {
  async doLogin({ commit }, payload) {
    commit('SET_LOADING', true, { root: true })
    try {
      const response = await this.$api.auth.login(payload)
      return response
    } catch (error) {
      throw error
    } finally {
      commit('SET_LOADING', false, { root: true })
    }
  },
}

export default {
  name,
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}