import Auth from './auth'
import Partner from './partner'

export default {
  [Auth.name]: Auth,
  [Partner.name]: Partner,
}