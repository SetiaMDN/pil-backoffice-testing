const name = 'partner'

const state = () => ({})

const getters = {}

const mutations = {}

const actions = {}

export default {
  name,
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
