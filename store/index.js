import modules from './modules'

export default {
    state() {
        return {
            loading: false,
            menus: [
                {
                    title: "Dashboard",
                    url: "/admin/dashboard",
                    icon: "icon-home",
                    hasChild: false,
                },
                {
                    title: "Users & Roles",
                    icon: "icon-users",
                    url: "/admin/users_roles",
                    hasChild: true,
                    children: [
                        {
                            title: "User",
                            url: "/admin/users_roles/users",
                            permission: "user.list"
                        
                        },
                        {
                            title: "Roles",
                            url: "/admin/users_roles/roles",
                            permission: "role.list"
                          
                        },
                        {
                            title: "Permission",
                            url: "/admin/users_roles/permissions",
                            permission: "permission.list"
                           
                        }
                    ]

                },
                {
                    title: "Partner",
                    icon: "icon-briefcase",
                    url: "/admin/partner_management/partner",
                    hasChild: false,

                },
                {
                    title: "Customer",
                    icon: "icon-user",
                    hasChild: false,
                    url: "/admin/customer",
                },
                {
                    title: "Audit Trails",
                    icon: "icon-activity",
                    hasChild: false,
                    url: "/admin/audit_trails",
                },
                {
                    title: "Reports",
                    icon: "icon-file-text",
                    url: "/admin/reports",
                    hasChild: true,
                    children: [
                        {
                            title: "Transaction History",
                            url: "/admin/reports/transactions",
                           
                        },
                    ]

                },
            ]
        }
    },
    getters: {},
    mutations: {
        SET_LOADING(state, payload) { state.loading = payload }
    },
    actions: {},
    modules
}
