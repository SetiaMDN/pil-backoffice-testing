const handleRedirect = function (route, redirect) {
    // if (route.path.includes('admin')) {
    //     return redirect('/admin/login')
    // }
    // return redirect('/admin/login')
}

export default async function ({ app, route, redirect, store }) {
    try {
        const check = await app.$api.auth.check()
        if (check.data.user.actionRequire === 'CHANGE_PASSWORD' && !route.path.includes('reset')) {
            if (route.path.includes('admin')) {
                return redirect('/admin/reset_password')
            }
            return redirect('/reset_password')
        } else {
            const {
                user,
                role,
                permissions
            } = check.data

            let roleObj = {
                id: user.RoleId,
                name: role,
            }

            store.commit('auth/SET_USER', user)
            store.commit('auth/SET_ROLE', roleObj)
            store.commit('auth/SET_PERMISSIONS', permissions)
        }
    } catch (error) {
        handleRedirect(route, redirect)
    }
}
