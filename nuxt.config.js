
require('dotenv').config();

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  server: {
    host: '0.0.0.0'
  },
  head: {
    title: 'PIL',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/digiasia-logo.jpeg' }
    ],
    /*
    ** Global JS
    */
    script: [
      /* Vendor */
      { src: '/vendors/js/vendors.min.js' },
      /* Pages */
      { src: '/vendors/js/ui/jquery.sticky.js' },
      { src: '/vendors/js/extensions/shepherd.min.js' },
      /* Themes */
      { src: '/js/core/app-menu.js' },
      { src: '/js/core/app.js' },
      { src: '/js/scripts/components.js' },
      {
        src: '/vendors/js/tables/datatable/pdfmake.min.js'
      },
      {
        src: '/vendors/js/tables/datatable/vfs_fonts.js'
      },
      {
        src: '/vendors/js/jszip/jszip.min.js'
      },
      {
        src: '/vendors/js/tables/datatable/datatables.min.js'
      },
      {
        src: '/vendors/js/tables/datatable/datatables.buttons.min.js'
      },
      {
        src: '/vendors/js/tables/datatable/buttons.html5.min.js'
      },
      {
        src: '/vendors/js/tables/datatable/buttons.print.min.js'
      },
      {
        src: '/vendors/js/tables/datatable/buttons.bootstrap.min.js'
      },
      {
        src: '/vendors/js/tables/datatable/datatables.bootstrap4.min.js'
      },
    ],
  },
  /*
  ** Global CSS
  */
  css: [
    /*
    ** Vendors
    */
    { src: '~/assets/vendors/css/vendors.min.css' },
    /*
    ** Theme
    */
    { src: '~/assets/css/bootstrap.css' },
    { src: '~/assets/css/bootstrap-extended.css' },
    { src: '~/assets/css/colors.css' },
    { src: '~/assets/css/components.css' },
    /*
    ** Pages
    */
    { src: '~/assets/css/core/menu/menu-types/horizontal-menu.css' },
    /*
    ** Custom
    */
    { src: '~/assets/css/styles.css' }
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '~/plugins/add_resources.js',
    '~/plugins/datatable.js',
    { src: '~/plugins/v-calendar.js', ssr: false },
    '@/plugins/axios',
    '@/plugins/api',
    '@/plugins/filters',
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'vue-sweetalert2/nuxt',
    '@nuxtjs/dotenv',
  ],
  /*
  ** Server Middleware
  */
  serverMiddleware: [
    '~/api/utils/logger.js',
    '~/api'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.APP_BASE_URL
  },
  /*
  ** Router configuration
  */
  router: {
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-active',
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {}
}
