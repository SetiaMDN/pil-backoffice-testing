FROM node:10.15.3-alpine

# Create app directory
RUN mkdir -p /root/app
WORKDIR /root/app

# Install app dependencies
RUN apk update && apk upgrade && apk add git

COPY package*.json ./
COPY . .
RUN npm i 

# Build app
RUN npm run build

EXPOSE 8080

# start command
CMD [ "npm", "start" ]

