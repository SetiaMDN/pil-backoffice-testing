export default (context, inject) => {
    const addResource = (type, url) => {
        if (type == 'js') {
            var el = document.querySelector(`script[src='${url}']`)
            if (el) {
                el.parentNode.removeChild(el);
            }
            let recaptchaScript = document.createElement("script")
            recaptchaScript.setAttribute("src", url)
            document.body.appendChild(recaptchaScript)
        } else {
            var isExist = document.querySelector(`link[href='${url}']`)

            if (!isExist) {
                let recaptchaStyle = document.createElement("link")
                recaptchaStyle.setAttribute("rel", "stylesheet")
                recaptchaStyle.setAttribute("href", url)
                document.head.appendChild(recaptchaStyle)
            }
        }
    }

    inject('addResource', addResource)
}