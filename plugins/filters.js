import Vue from 'vue'
import moment from 'moment-timezone';

export default ({ }, inject) => {
  Vue.filter('formattedDateTime', (
    dateTime,
    dateTimeFormat = 'DD MMMM YYYY - HH:mm A',
    timezone = 'Asia/Jakarta'
  ) => {
    if (!dateTime) return '-'

    return moment(dateTime)
      .tz(timezone)
      .format(dateTimeFormat)
  })

  Vue.filter('formattedAddDate', (
    dateValue,
    addValue = 1,
    addTypeValue = 'days',
    dateFormat = 'YYYY-MM-DD'
  ) => {
    if (!dateValue) return ''

    return moment(dateValue)
      .add(addValue, addTypeValue)
      .format(dateFormat)
  })

  Vue.filter('formattedNumber', (
    number,
    separator = ','
  ) => {
    let formattedNumber = 0

    if (number) {
      formattedNumber = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator)
    }

    return `IDR ${formattedNumber}`
  })

  // to use this you must use watcher & input type text
  Vue.filter('formatInputNumber', (
    number,
    separator = ','
  ) => number.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, separator))

  Vue.filter('unformatNumber', (
    value
  ) => parseInt(value.toString().replace(/\D/g, "")))

  inject('filters', Vue.options.filters)
}
