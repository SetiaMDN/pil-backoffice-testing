import qs from 'query-string'

export default async ({ $axios, store, redirect }) => {
    $axios.onResponse(res => {
        return res.data
    })

    $axios.defaults.paramsSerializer = params => {
        return qs.stringify(params)
    }

    $axios.defaults.headers['Content-Type'] = 'application/json';
}
