export default (context, inject) => {
    const initiateDataTable = (scrollX) => {
        $('.dataex-html5-selectors').DataTable({
            dom: '<"top"lfB>rt<"bottom"ip><"clear">',
            scrollX,
            buttons: ['excel', 'pdf'],
            ordering: false
        });
    };

    const initiateDataTableCashier = () => {
        $('.dataex-html5-selectors').DataTable();
    };

    inject('initiateDataTable', initiateDataTable)
    inject('initiateDataTableCashier', initiateDataTableCashier)
}