import authModule from '~/modules/auth'
import adminsModule from '~/modules/admins'
import utilsModule from '~/modules/utils'
import passwordModule from '~/modules/password'
import partnerModule from '~/modules/partner'

export default ({ $axios, env }, inject) => {
    // inject the api in the context (ctx.app.$api)
    // And in the Vue instances (this.$api in your components)
    const api = {
        auth: authModule($axios),
        admins: adminsModule($axios),
        utils: utilsModule($axios),
        password: passwordModule($axios),
        partner: partnerModule($axios)
    }

    inject('api', api)
}
