const { Router } = require('express')
const axios = require('../axios.js')

const router = Router()
const partner_url = process.env.PARTNER_BASE_URL;

const getDetail = async (req, res) => {
    try {
        let response = await axios.get(`${partner_url}/auth/user`)
        res.status(200).json(response.data.payload)
    }
    catch (err) {
        res.status(500).json(err)
    }
}

const forgotPassword = async (req, res) => {
    try {
        let response = await axios.post(`${partner_url}/auth/password/forgot`, req.body)
        if (response.status == 200) {
            res.status(200).json({ status: 'success' });
        }

        res.status(400).json(response.data)
    }
    catch (err) {
        res.status(400).json(err.response.data)
    }
}

const requestChangePassword = async (req, res) => {
    try {
        let response = await axios.post(`${partner_url}/auth/password/change`, req.body)
        if (response.status == 200) {
            res.status(200).json({ status: 'success' });
        }
    }
    catch (err) {
        res.status(500).json(err)
    }
}

const updateProfile = async (req, res) => {
    try {
        let response = await axios.put(`${partner_url}/auth/user`, req.body)

        if (response.status == 200) {
            res.status(200).json({ status: 'success' });
        }

        res.status(400).json(response.data)
    }
    catch (err) {
        if (err.response.status == 500) {
            res.status(500).json(err)
        }
        res.status(400).json(err.response.data)
    }
}

const confirmChangePassword = async (req, res) => {
    try {
        let response = await axios.post(`${partner_url}/auth/password/change/confirm`, req.body)
      
        if (response.status == 200) {
            res.status(200).json({ status: 'success' });
        }

        res.status(400).json(response.data)
    }
    catch (err) {
       
        res.status(500).json(err)
    }
}

const confirmForgotPass = async (req, res) => {
    const { token, ...restParams } = req.body;
    const config = {
        headers: { Authorization: `Bearer ${token}` }
    };
    try {
        let response = await axios.post(`${partner_url}/auth/password/forgot/confirm`, restParams, config);
        console.log('response-->', response);

        if (response.status == 200) {
            res.status(200).json({ status: 'success' });
        }

        res.status(400).json(response.data)
    }
    catch (err) {
       
        res.status(500).json(err)
    }
}

router.put('/update', updateProfile);
router.post('/forgot_password', forgotPassword);
router.post('/change_password/request', requestChangePassword);
router.post('/change_password/confirm', confirmChangePassword);
router.get('/get_detail', getDetail);
router.post('/confirm_forgot_password', confirmForgotPass);

module.exports = router
