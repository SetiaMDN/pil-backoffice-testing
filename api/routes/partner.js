const { Router } = require('express')
const axios = require('../axios')
const { checkParams } = require("../utils");

const baseUrl = process.env.PARTNER_BASE_URL;
const transactionUrl = process.env.TRANSACTIONS_BASE_URL;
const auditTrailUrl = process.env.AUDIT_TRAIL_URL;
const router = Router()

// partner
const getPartnerList = async (req, res) => {
  try {
    const response = await axios.get(
      `${baseUrl}/partner${checkParams(req.query)}`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const createPartner = async (req, res) => {
  try {
    const response = await axios.post(
      `${baseUrl}/partner`, req.body
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const deletePartner = async (req, res) => {
  try {
    const accountNumber = req.params.id;
    const response = await axios.delete(
      `${baseUrl}/partner/${accountNumber}`, 
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const activated = async (req, res) => {
  try {
    const accountNumber = req.params.id;
    const response = await axios.patch(
      `${baseUrl}/partner/${accountNumber}/activate`, 
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const deactivated = async (req, res) => {
  try {
    const accountNumber = req.params.id;
    const response = await axios.patch(
      `${baseUrl}/partner/${accountNumber}/deactivate`, 
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const getDetail = async (req, res) => {
  try {
    const response = await axios.get(
      `${baseUrl}/partner/${req.params.id}`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};


const countries = async (req, res) => {
  try {
    let response = await axios.get(`${baseUrl}/area/country`);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};
const showCountry = async (req, res) => {
  const id = req.params.id;

  try {
    let response = await axios.get(`${baseUrl}/area/country/${id}`);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const provinces = async (req, res) => {
  try {
    let response = await axios.get(
      `${baseUrl}/area/province?countryId=${req.query.countryId}`
    );
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const showProvince = async (req, res) => {
  const id = req.params.id;
  try {
    let response = await axios.get(`${baseUrl}/area/province/${id}`);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const cities = async (req, res) => {
  try {
    let response = await axios.get(
      `${baseUrl}/area/city?provinceId=${req.query.provinceId}`
    );
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const showCity = async (req, res) => {
  const id = req.params.id;
  try {
    let response = await axios.get(`${baseUrl}/area/city/${id}`);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const districts = async (req, res) => {
  try {
    let response = await axios.get(
      `${baseUrl}/area/sub_urban?cityId=${req.query.cityId}`
    );
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const showDistrict = async (req, res) => {
  const id = req.params.id;
  try {
    let response = await axios.get(`${baseUrl}/area/sub_urban/${id}`);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const villages = async (req, res) => {
  try {
    let response = await axios.get(
      `${baseUrl}/area/area?subUrbanId=${req.query.districtId}`
    );
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const showVillage = async (req, res) => {
  const id = req.params.id;
  try {
    let response = await axios.get(`${baseUrl}/area/area/${id}`);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};
const getCustomer = async (req, res) => {
  const payload = req.body;
  try {
    let response = await axios.post(`${baseUrl}/customer/list`, payload);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};

const getCustomerDetail = async (req, res) => {
  try {
    let response = await axios.get(`${baseUrl}/customer/detail/${req.params.id}`);
    res.status(200).json(response.data.payload);
  } catch (err) {
    res.status(500).json(err);
  }
};
const updateCustomer = async (req, res) => {
  try {
    let response = await axios.put(`${baseUrl}/customer/${req.params.id}`, req.body);
    res.status(200).json(response.data);
  } catch (err) {
    res.status(500).json(err);
  }
};
const getCustomerPartner = async (req, res) => {
  try {
    let response = await axios.get(`${baseUrl}/customer/${req.params.accountNumber}/partner`);
    res.status(200).json(response.data.payload.data);
  } catch (err) {
    res.status(500).json(err);
  }
};
const updatePartner = async (req, res) => {
  try {
    let response = await axios.put(`${baseUrl}/partner/${req.params.id}`, req.body);
    res.status(200).json(response.data);
  } catch (err) {
    res.status(500).json(err);
  }
};
const getRoleList = async (req, res) => {
  try {
    const response = await axios.get(
      `${baseUrl}/role${checkParams(req.query)}`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const createRole = async (req, res) => {
  try {
    const response = await axios.post(
      `${baseUrl}/role`, req.body
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const updateRole = async (req, res) => {
  const id = req.params.id
  try {
    const response = await axios.put(
      `${baseUrl}/role/${id}`, req.body
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const deleteRole = async (req, res) => {
  const id = req.params.id
  try {
    const response = await axios.delete(
      `${baseUrl}/role/${id}`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const getRole = async (req, res) => {
  const id = req.params.id
  try {
    const response = await axios.get(
      `${baseUrl}/role/${id}`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const listUser = async (req, res) => {
  try {
    const response = await axios.get(
      `${baseUrl}/user${checkParams(req.query)}`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const createUser = async (req, res) => {
  try {
    const response = await axios.post(
      `${baseUrl}/user`, req.body
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const getUser = async (req, res) => {
  const id = req.params.id
  try {
    const response = await axios.get(
      `${baseUrl}/user/${id}`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const updateUser = async (req, res) => {
  const id = req.params.id
  try {
    const response = await axios.put(
      `${baseUrl}/user/${id}`, req.body
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const listPermission = async (req, res) => {
  try {
    const response = await axios.get(
      `${baseUrl}/permission`
    );
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const getCustomerByPartner = async (req, res) => {
  const id = req.params.id;
  try {
    const response = await axios.post(
      `${baseUrl}/partner/${id}/customer`, req.body
    );
    console.log('response-->', response);
    return res.status(200).json(response.data.payload);
  } catch (err) {
    return res.status(500).json(err);
  }
};

const activatedCustomer = async (req, res) => {
  try {
    const accountNumber = req.params.id;
    const response = await axios.patch(
      `${baseUrl}/customer/${accountNumber}/activate`, 
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const deactivatedCustomer = async (req, res) => {
  try {
    const accountNumber = req.params.id;
    const response = await axios.patch(
      `${baseUrl}/customer/${accountNumber}/deactivate`, 
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};

const activatedCustomerByPartner = async (req, res) => {
  try {
    const partnerId = req.params.partnerId;
    const customerId = req.params.customerId;
    const response = await axios.patch(
      `${baseUrl}/partner/${partnerId}/customer/${customerId}/activate`, 
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const deactivatedCustomerByPartner = async (req, res) => {
  try {
    const partnerId = req.params.partnerId;
    const customerId = req.params.customerId;
    const response = await axios.patch(
      `${baseUrl}/partner/${partnerId}/customer/${customerId}/deactivate`, 
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const getAuditTrails = async (req, res) => {
  try {
    const response = await axios.get(
      `${auditTrailUrl}/audit_trails`
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};


const getTransactions = async (req, res) => {
  try {
    const response = await axios.post(
      `${transactionUrl}/transactions`,
      req.body
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};

const exportTransactions = async (req, res) => {
  // try {
  //   const response = await axios.post(
  //     `${transactionUrl}/transactions`,
  //     req.body
  //   );
  //   return res.status(200).json(response.data);
  // } catch (err) {
  //   return res.status(500).json(err);
  // }
  const query = pick(req.query, [
    "companyId",
    "branchId",
    "cashierId",
    "from",
    "to",
    "remitType",
    "hitAccountStatus"
  ]);

  const params = {
    ...query,
    page: req.query.page || 1,
    perPage: req.query.limit || 25
  };

  const response = await new requestReportTransactions(req).exports(params);
  const { downloadPathUrl } = response.data;

  return res.json(downloadPathUrl);
};


const getAllProduct = async (req, res) => {
  try {
    const response = await axios.get(
      `${baseUrl}/product`
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const getProductAccess = async (req, res) => {
  try {
    const response = await axios.get(
      `${baseUrl}/partner/product/byPartner/${req.params.id}`
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};
const updateProductAccessByPartner = async (req, res) => {
  try {
    const response = await axios.put(
      `${baseUrl}/partner/${req.params.id}/product`, req.body
    );
    return res.status(200).json(response.data);
  } catch (err) {
    return res.status(500).json(err);
  }
};

// PARTNER
router.get('/list', getPartnerList);
router.post('/create', createPartner);
router.delete('/delete/:id', deletePartner);
router.patch('/activated/:id', activated);
router.patch('/deactivated/:id', deactivated);
router.get('/detail/:id', getDetail);
router.put('/partner/:id', updatePartner);


router.get("/countries", countries);
router.get("/provinces", provinces);
router.get("/cities", cities);
router.get("/districts", districts);
router.get("/villages", villages);
router.get("/countries/:id", showCountry);
router.get("/provinces/:id", showProvince);
router.get("/cities/:id", showCity);
router.get("/districts/:id", showDistrict);
router.get("/villages/:id", showVillage);


// CUSTOMER
router.post('/customer/list', getCustomer);
router.get('/customer/:id', getCustomerDetail);
router.put('/customer/:id', updateCustomer);
router.get('/customer/:accountNumber/partner', getCustomerPartner);
router.post('/customer/:id/customer', getCustomerByPartner);
router.patch('/activated-customer/:id', activatedCustomer);
router.patch('/deactivated-customer/:id', deactivatedCustomer);
router.patch('/activated-customer-partner/:partnerId/:customerId', activatedCustomerByPartner);
router.patch('/deactivated-customer-partner/:partnerId/:customerId', deactivatedCustomerByPartner);


// ROLE
router.get('/role', getRoleList);
router.post('/role', createRole);
router.put('/role/:id', updateRole);
router.delete('/role/:id', deleteRole);
router.get('/role/:id', getRole);


// USER
router.get('/user', listUser);
router.post('/user', createUser);
router.get('/user/:id', getUser);
router.put('/user/:id', updateUser);


// PERMISSION
router.get('/permission', listPermission);

// AUDIT TRAILS
router.get('/auditTrails', getAuditTrails);

// TRANSACTIONS
router.post('/transactions', getTransactions);
router.post('/exportTransactions', exportTransactions);


// PRODUCT
router.get('/product', getAllProduct);
router.get('/:id/product', getProductAccess);
router.put('/:id/product', updateProductAccessByPartner);


module.exports = router;
