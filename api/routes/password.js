const { Router } = require('express')
const axios = require('../axios.js')
const { checkParams } = require('../utils')

const router = Router()
const base_url = process.env.AUTH_BASE_URL

// PASSWORD LIST
const list = async (req, res) => {
  try {
    let response = await axios.get(`${base_url}/password-configurations${checkParams(req.query)}`)
    res.status(200).json(response.data)
  }
  catch (err) {
    res.status(500).json(err)
  }
}

// CREATE PASSWORD
const create = async (req, res) => {
  try {
    let response = await axios.post(`${base_url}/password-configurations`, req.body)
    if (response.data.message) {
      res.status(400).json(response.data)
    }
    res.status(200).json(response.data)
  }
  catch (err) {
    res.status(500).json(err)
  }
}

// UPDATE PASSWORD
const update = async (req, res) => {
  const id = req.params.id
  try {
    let response = await axios.put(`${base_url}/password-configurations/${id}`, req.body)
    if (response.data.message) {
      res.status(400).json(response.data)
    }
    res.status(200).json(response.data)
  }
  catch (err) {
    res.status(500).json(err)
  }
}

router.get('/', list)
router.post('/', create)
router.put('/:id', update)

module.exports = router
