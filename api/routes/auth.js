const { Router } = require('express')
const passport = require('../passport.js')
// const { encrypt, decrypt } = require('../crypto')
const router = Router()

const adminLogin = (req, res, next) => {
    passport.authenticate("local", (err, user, info) => {
        if (err) {
            return res.status(500).json(err.response)
        }

        if (!user) {
            return res.status(400).json(info)
        }

        req.login(user, err => {
            if (err) { return next(err) }
            return res.status(200).json({ message: 'success logged in' })
        })
    })(req, res, next)
}

const adminCheck = (req, res) => {
    if (!req.isAuthenticated()) {
        return res.json({ message: 'NotAuthenticated' }).status(400)
    } else {
        return res.json({ data: req.user })
    }
}

const adminLogout = (req, res) => {
    req.logout();
    console.log('logout success')
    return res.status(200).json({ message: 'Logout Success' })
}


router.post('/admin/login', adminLogin)
router.get('/admin/check', adminCheck)
router.get('/admin/logout', adminLogout)

module.exports = router
