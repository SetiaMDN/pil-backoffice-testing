import XLSX from 'xlsx';
const { Router } = require('express')
const multer = require('multer')
const fs = require('fs')
const { get } = require('lodash')
const request = require('superagent')
const router = Router()

const base_url = process.env.UTILS_BASE_URL

const storage = multer.diskStorage({
    destination: "uploads",
    filename: (req, file, cb) => {
        return cb(
            null,
            file.originalname
        )
    },
})

const upload = multer({
    storage
})

const uploadFile = async (req, res) => {
    const file = req.file
    const filePath = __base + '/uploads/' + file.filename
    const dir = req.body.dir || 'default'
        
    try {
        const response = await request.post(`${base_url}/backblaze/upload`)
                        .attach('file', fs.createReadStream(filePath))
                        .field('dir', dir)

        if (get(response, 'data.message')) {
            res.status(400).json(response.data)
        }

        res.status(200).json(response.body || response.text)
    }
    catch (err) {
        if (get(err, 'response.status') == 500) {
            res.status(500).json(err)
        }
        console.log(err)
        res.json(err.response)
    } finally {
        fs.unlinkSync(filePath)
    }
}

const exportsCSV = async (req, res) => {
    const data = req.body;
    const trxJson = data.map((item) => ({
      'Partner Name': item.partnerName || '',
      'Partner Account Number': item.partner || '',
      'Time': item.extra && item.extra.date ? item.extra.date : '',
      'Type': item.type || '',
      'Sender': item.origin || '',
      'Recipient': item.destination || '',
      'Total Amount': item.extra && item.extra['wallet-transactions'] && item.extra['wallet-transactions'].debit ? 
      item.extra['wallet-transactions'].debit : 0,
      'Status': item.status || '',
      'Retrieval Reference': item.retrievalReference || '',
    }));
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(trxJson);
    ws['!cols'] = [
      { wch: 8 },
      { wch: 30 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
      { wch: 15 },
    ];
    
    XLSX.utils.book_append_sheet(wb, ws, 'Transactions Report');
    const wbBuffer = XLSX.write(wb, {
      type: 'base64',
    });
    res.writeHead(200, [
      [
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      ],
    ]);
    return res.end(Buffer.from(wbBuffer, 'base64'));
}



router.post('/upload', upload.single('file'), uploadFile)
router.post('/exportCsv', exportsCSV)

module.exports = router
