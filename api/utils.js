const checkParam = (param, last) => {
    if (param[1]) return `${param[0]}=${param[1]}${last ? '' : '&'}`
    else return ''
}

const checkParams = (queryParams) => {
    const params = Object.entries(queryParams)
    let query = '?'

    params.forEach((p, i) => query += checkParam(p, params.length == ++i) )
    
    return query
}

module.exports = { checkParams }