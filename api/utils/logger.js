import moment from 'moment';

export default function (req, res, next) {
  const urlSplit =  req.url.split('/').join('.').split('.');
  const notAllowedLog = ['api', 'css', 'js', 'map', 'json', '_nuxt', '__webpack_hmr', 'jpg', 'png', 'gif', 'svg'];
  const result = urlSplit.filter(uri => notAllowedLog.includes(uri));

  if(result.length == 0 && req.url != '/ping') {
      console.log(`[Client] ${moment().format('DD/MM/YYYY HH:mm:ss')} - ${req.url} - ${req.headers['user-agent']}`);
  }

  next();
}