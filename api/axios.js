const qs = require("query-string")
const axios = require("axios")

axios.defaults.paramsSerializer = params => {
    return qs.stringify(params)
}

axios.defaults.headers['Content-Type'] = 'application/json';

module.exports = axios
