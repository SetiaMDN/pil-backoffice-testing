const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const axios = require('./axios')
const { encrypt } = require('./crypto')

// const base_url = process.env.AUTH_BASE_URL
const base_url = process.env.PARTNER_BASE_URL;

passport.use(new LocalStrategy(
    async function (username, password, done) {
        try {
            // let response = await axios.post(`${base_url}/oauth/admin/token`, { username, password })
            let response = await axios.post(`${base_url}/auth/login`, { username, password })
            const token = response.data.payload.auth.accessToken;
            const encryptedToken = encrypt(token);
            const user = {
                token: token,
                expires_in : response.data.payload.auth.expiresIn,
                refresh_expires_in: response.data.payload.auth.refreshExpiresIn,
                user : response.data.payload.user
            };
            user.token = encryptedToken
            return done(null, user)
        } catch (error) {
            if (error.response) {
                if (error.response.status == 500) return done(error.response.data)
                return done(null, false, error.response.data)
            }
            else return done(error)
        }
    }
))

passport.serializeUser((user, done) => {
    done(null, user)
})

passport.deserializeUser((user, done) => {
    done(null, user)
})

module.exports = passport