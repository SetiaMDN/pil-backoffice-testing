const express = require('express')
const bodyParser = require("body-parser")
const cookieSession = require('cookie-session')
const { decrypt } = require('./crypto')
const axios = require('./axios')

const passport = require('./passport')

// Create express instance
const app = express()

// Require API routes
const auth = require('./routes/auth')
const admins = require('./routes/admins')
const partner = require('./routes/partner')
const utils = require('./routes/utils')

const Winston =  require('winston');
const expressWinston = require('express-winston');

app.use(
  expressWinston.logger({
    transports: [
      new Winston.transports.Console()
    ]
  })
);

global.__base = __dirname.replace('/api', '')

//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cookieSession({
  name: process.env.AUTH_COOKIE_NAME,
  keys: [process.env.AUTH_COOKIE_KEY],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(async (req, res, next) => {
  if (req.user) {
    const token = decrypt(req.user.token)
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
  } else {
    req.logout()
  }
  next()
})

// Import API Routes
app.use('/auth', auth)
app.use('/admins', admins)
app.use('/partner', partner)
app.use('/utils', utils)

app.get(/^\/(api).*/, function (req, res) {
  res.status(404)
})

// Export express app
module.exports = {
  path: '/api',
  handler: app
}

// Start standalone server if directly running
// if (require.main === module) {
//   const port = process.env.PORT || 3001
//   app.listen(port, () => {
//     console.log(`API server listening on port ${port}`)
//   })
// }
