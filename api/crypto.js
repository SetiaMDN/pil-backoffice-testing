var CryptoJS = require("crypto-js")

const encrypt = (text) =>
    CryptoJS.AES.encrypt(text, process.env.CRYPTO_SECRET_KEY).toString()

const decrypt = (cipherText) => {
    const bytes = CryptoJS.AES.decrypt(cipherText, process.env.CRYPTO_SECRET_KEY)
    return bytes.toString(CryptoJS.enc.Utf8)
}

module.exports = { encrypt, decrypt }