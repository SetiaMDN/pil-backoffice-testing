#!/bin/sh

cd $(dirname $0)
PACKAGE_VERSION="$(git log --format='%H' -n 1)"
echo "deploying image version: "$PACKAGE_VERSION
export PACKAGE_VERSION=$PACKAGE_VERSION
envsubst '${PACKAGE_VERSION}' < deploy.yaml | kubectl apply -f -
echo "completed with status code: "$?
