#!/bin/sh

DOCKER_REGISTRY="registry-intl.ap-southeast-5.aliyuncs.com/digiasia"
IMAGE_NAME="pil-backoffice"
PACKAGE_VERSION="$(git log --format='%H' -n 1)"
echo "building image version: "$DOCKER_REGISTRY/$IMAGE_NAME:$PACKAGE_VERSION
docker build -t $IMAGE_NAME .
docker tag $IMAGE_NAME:latest $DOCKER_REGISTRY/$IMAGE_NAME:latest
docker tag $IMAGE_NAME:latest $DOCKER_REGISTRY/$IMAGE_NAME:$PACKAGE_VERSION
docker push $DOCKER_REGISTRY/$IMAGE_NAME:latest
docker push $DOCKER_REGISTRY/$IMAGE_NAME:$PACKAGE_VERSION
