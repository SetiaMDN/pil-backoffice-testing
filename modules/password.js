const path = '/api/password'

export default axios => ({
  async create(data) {
    return await axios.post(path, data)
  },
  async update(id, data) {
    return await axios.put(path + '/' + id, data)
  },
  async list(query) {
    return await axios.get(path + '?' + query)
  },
})
