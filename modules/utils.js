import fileSaver from 'file-saver';
import moment from 'moment';
const path = '/api/utils'

export default axios => ({
    async upload(data) {
        return await axios({
            url: `${path}/upload`,
            method: 'post',
            data,
            headers: { 'Content-Type': 'multipart/form-data' }
        })
    },
    async exportCsv(data) {
      const exportResponse = await axios.post(path + '/exportCsv', data, { responseType: 'blob' });
      const now = moment().format('YYMMDDHHmmSSSS');

      const blob = new Blob([exportResponse.data], {
        type: 'vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8',
      });
      fileSaver.saveAs(blob, `transactions-report-${now}.xlsx`);
    },
})
