const path = '/api/partner'

export default axios => ({

  // PARTNER
  async getPartnerList(filter) {
    return await axios.get(`${path}/list?page=${filter.page}&perPage=${filter.perPage}`)
  },
  async createPartner(data) {
    return await axios.post(`${path}/create`, data)
  },
  async deletePartner(id) {
    return await axios.delete(`${path}/delete/${id}`)
  },
  async activated(id) {
    return await axios.patch(`${path}/activated/${id}`)
  },
  async deactivated(id) {
    return await axios.patch(`${path}/deactivated/${id}`)
  },
  async getDetail(id) {
    return await axios.get(`${path}/detail/${id}`)
  },
  async updatePartner(id,data) {
    return await axios.put(`${path}/partner/${id}`, data);
  },




  async countries() {
    return await axios.get(`${path}/countries`);
  },
  async showCountry(id) {
    return await axios.get(`${path}/countries/${id}`);
  },
  async provinces(countryId) {
    return await axios.get(`${path}/provinces?countryId=${countryId}`);
  },
  async showProvince(id) {
    return await axios.get(`${path}/provinces/${id}`);
  },
  async cities(provinceId) {
    return await axios.get(`${path}/cities?provinceId=${provinceId}`);
  },
  async districts(cityId) {
    return await axios.get(`${path}/districts?cityId=${cityId}`);
  },
  async villages(districtId) {
    return await axios.get(`${path}/villages?districtId=${districtId}`);
  },
  async showCity(id) {
    return await axios.get(`${path}/cities/${id}`);
  },
  async showDistrict(id) {
    return await axios.get(`${path}/districts/${id}`);
  },
  async showVillage(id) {
    return await axios.get(`${path}/villages/${id}`);
  },


  // CUSTOMER
  async getCustomer(params) {
    return await axios.post(`${path}/customer/list`, params);
  },
  async activatedCustomer(id) {
    return await axios.patch(`${path}/activated-customer/${id}`)
  },
  async activatedCustomerByPartner(customerId, partnerId) {
    return await axios.patch(`${path}/activated-customer-partner/${partnerId}/${customerId}`)
  },
  async deactivatedCustomer(id) {
    return await axios.patch(`${path}/deactivated-customer/${id}`)
  },
  async deactivatedCustomerByPartner(customerId, partnerId) {
    return await axios.patch(`${path}/deactivated-customer-partner/${partnerId}/${customerId}`)
  },
  async getCustomerDetail(id) {
    return await axios.get(`${path}/customer/${id}`);
  },
  async updateCustomer(id,data) {
    return await axios.put(`${path}/customer/${id}`, data);
  },
  async getCustomerPartner(accountNumber) {
    return await axios.get(`${path}/customer/${accountNumber}/partner`);
  },
  async getCustomerByPartner(params) {
    return await axios.post(`${path}/customer/${params.id}/customer`, params);
  },



  // ROLE
  async getRoleList(filter) {
    return await axios.get(`${path}/role?page=${filter.page}&perPage=${filter.perPage}`)
  },
  async createRole(data) {
    return await axios.post(`${path}/role`,data)
  },
  async updateRole(id, data) {
    return await axios.put(`${path}/role/${id}`, data)
  },
  async deleteRole(id) {
    return await axios.delete(`${path}/role/${id}`)
  },
  async getRole(id) {
    return await axios.get(`${path}/role/${id}`)
  },


  // USER
  async listUser(filter) {
    return await axios.get(`${path}/user?page=${filter.page}&perPage=${filter.perPage}`)
  },
  async createUser(data) {
    return await axios.post(`${path}/user`, data)
  },
  async getUser(id) {
    return await axios.get(`${path}/user/${id}`)
  },
  async updateUser(id, data) {
    return await axios.put(`${path}/user/${id}`, data)
  },

  // PERMISSION
  async listPermission(filter) {
    return await axios.get(`${path}/permission`)
  },

  // AUDIT TRAILS
  async getAuditTrails(payload){
    return await axios.get(`${path}/auditTrails?page=${payload.page}&limit=${payload.perPage}`)
  },

  // TRANSACTIONS
  async getTransactions(payload){
    return await axios.post(`${path}/transactions`, payload)
  },
  async exportFileExcelTransactions(payload){
    return await axios.post(`${path}/exportTransactions`, payload)
  },

  // PRODUCT
  async getAllProduct(){
    return await axios.get(`${path}/product`)
  },
  async getProductAccess(id){
    return await axios.get(`${path}/${id}/product`)
  },
  async updateProductAccessByPartner(partnerId, productAccess){
    return await axios.put(`${path}/${partnerId}/product`, productAccess)
  }

})
