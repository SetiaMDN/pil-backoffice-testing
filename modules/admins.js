const path = '/api/admins'

export default axios => ({
    async update(data) {
        return await axios.put(`${path}/update`, data)
    },
    async forgotPassword(data) {
        return await axios.post(`${path}/forgot_password`, data)
    },
    async changePassword(data) {
        return await axios.post(`${path}/change_password/request`, data)
    },
    async confirmPassword(data) {
        return await axios.post(`${path}/change_password/confirm`, data)
    },
    async get(){
        return await axios.get(`${path}/get_detail`)
    },
    async confirmForgot(data){
        return await axios.post(`${path}/confirm_forgot_password`, data)
    }
})
