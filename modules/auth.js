const path = '/api/auth/admin'

export default axios => ({
    async login({ username, password }) {
        return await axios.post(path + '/login', { username, password })
    },
    async check() {
        return await axios.get(path + '/check')
    },
    async logout() {
        return await axios.get(path + '/logout')
    },
    
})
